<?php
function extractDivContent($html, $className) {
    $doc = new DOMDocument();
    libxml_use_internal_errors(true);
    $doc->loadHTML($html);
    libxml_clear_errors();

    $xpath = new DOMXPath($doc);
    $divList = $xpath->query("//div[contains(concat(' ', normalize-space(@class), ' '), ' $className ')]");

    $divContent = '';
    foreach ($divList as $div) {
        $divContent .= $doc->saveHTML($div);
    }

    return $divContent;
}

function hasChanged($oldHtml, $newHtml) {
    $oldHtmlNormalized = preg_replace('/\s+/', '', $oldHtml);
    $newHtmlNormalized = preg_replace('/\s+/', '', $newHtml);

    return ($oldHtmlNormalized !== $newHtmlNormalized);
}
?>
