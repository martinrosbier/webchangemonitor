# WebChangeMonitor

WebMonitor is a PHP script designed to monitor changes in a website's HTML content and send an email notification when specific criteria are met.

I created this project to facilitate scheduling an appointment to meet at 'La Cafetería El Molino.' I suspect that it's always unavailable. Let's check my suspicion!

## Table of Contents

- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [File Structure](#file-structure)
- [Unit Testing](#unit-testing)
- [Contributing](#contributing)
- [License](#license)

## Features

- Retrieves HTML content from a specified website using cURL.
- Detects changes between two versions of the website.
- Sends an email notification if changes are detected, and the new version contains next month.

## Installation

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/martinrosbier/webchangemonitor.git
    ```

2. Navigate to the project directory:

    ```bash
    cd webchangemonitor
    ```

3. Customize the configuration in `main.php` (website URL, email recipient, etc.).

4. Set up a cron job to run the script at regular intervals. For example, to run every 5 minutes:

    ```bash
    */5 * * * * /path/to/php /path/to/main.php
    ```

## Usage

Ensure that the cron job is set up to execute `main.php` at the desired frequency. The script will check for changes on the specified website and send an email if the conditions are met.

Feel free to change the url and the initialhtml and class to monitor, and it will work with any website you need to monitor.

## File Structure

- `inc/curl.php`: Contains the function to retrieve HTML content using cURL.
- `inc/change_detection.php`: Contains the function to detect changes between two HTML versions.
- `inc/email.php`: Contains the function to send an email notification.
- `main.php`: The main script that orchestrates the website monitoring process.
