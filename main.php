<?php
require_once 'inc/curl.php';
require_once 'inc/change_detection.php';
require_once 'inc/email.php';
require_once 'inc/initialhtml.php';

$websiteUrl = 'https://delmolino.gob.ar/inscripcion/index.html';

$newSource = getHtml($websiteUrl);

$className = "formularioInscripcion col-xs-12";

$newHtml = extractDivContent($newSource, $className);

if (hasChanged($initialHtml, $newHtml)) {
    $to = 'info@martinrosbier.com.ar';
    $subject = 'Website Change Alert';
    $message = 'The website has been updated';
    sendEmail($to, $subject, $message);
}