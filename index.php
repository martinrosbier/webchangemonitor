<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Suscripción a Notificaciones por Correo Electrónico</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <style>
        body {
            font-family: 'Montserrat', sans-serif;
            line-height: 1.6;
            margin: 20px;
            padding: 20px;
            max-width: 600px;
            margin: auto;
        }

        h1 {
            color: #333;
        }

        p {
            margin-bottom: 20px;
        }

        label {
            display: block;
            margin-bottom: 5px;
        }

        input {
            width: 70%;
            padding: 8px;
            margin-bottom: 10px;
            box-sizing: border-box;
        }

        button {
            background-color: #000;
            color: white;
            padding: 10px;
            border: none;
            cursor: pointer;
        }

        button:hover {
            background-color: #666;
        }

        .warning-box {
            background-color: #eee;
            color: #333;
            padding: 10px;
            border: 1px solid #eee;
            border-radius: 5px;
            margin-bottom: 20px;
        }

    </style>
</head>
<body>
    <h1>Suscripción a Notificaciones por Correo Electrónico</h1>

    <?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['email'])) {
            $email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);

            if ($email) {
                $to = 'info@martinrosbier.com.ar';
                $subject = 'Nuevo Suscriptor';
                $message = 'Nuevo suscriptor: ' . $email;

                mail($to, $subject, $message);

                echo '<div class="warning-box">¡Gracias por suscribirte! Te notificaremos cuando haya actualizaciones.</div>';
            } else {
                echo '<div class="warning-box">Dirección de correo electrónico no válida. Por favor, inténtalo de nuevo.</div>';
            }
        }
    }
    ?>
    <p>Existe algo llamado <a href="https://delmolino.gob.ar/inscripcion/index.html" target="_blank"><i>Experiencia Molino, un recorrido guiado</i></a> al cual me interesa ir, hace meses que<br>
    intento y siempre está agotado. Empecé a sospechar que nunca está el formulario habilitado, cree un script<br>
    para sacarme la duda. ¿Queres que te avise cuál es el resultado? Deja tu email debajo.</p>
    <form action="index.php" method="post">
        <label for="email">Correo Electrónico:</label>
        <input type="email" id="email" name="email" required>
        <button type="submit">Suscribirse</button>
    </form>
</body>
</html>
